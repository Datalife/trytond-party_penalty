# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from simpleeval import simple_eval
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.tools import decistmt
from trytond.i18n import gettext
from trytond.exceptions import UserError

__all__ = ['Party']


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    penalty_formula = fields.Char('Penalty formula',
        help=('Python expression that will be evaluated with:\n'
            '- untaxed_amount: untaxed amount\n'
            '- total_amount: total amount\n'
            '- quantity: quantity\n'))

    def check_penalty_formula(self):
        context = self.get_context_penalty_formula(field_values={})

        try:
            value = self.get_penalty_amount(**context)
            if (not isinstance(value, Decimal)
                    and not isinstance(Decimal(value), Decimal)):
                raise ValueError
        except ValueError as exception:
            raise UserError(gettext('msg_invalid_penalty_formula',
                formula=self.penalty_formula,
                party=self.rec_name,
                exception=exception))

    def get_context_penalty_formula(self, **field_values):
        return {'names': {
                'untaxed_amount': field_values.get('untaxed_amount', Decimal('0.0')),
                'total_amount': field_values.get('total_amount', Decimal('0.0')),
                'quantity': Decimal(field_values.get('quantity', '0.0'))
                },
        }

    def get_penalty_amount(self, **field_values):
        """Return penalty amount (as Decimal)"""
        if not self.penalty_formula:
            return Decimal('0.0')
        context = self.get_context_penalty_formula(**field_values)
        context.setdefault('functions', {})['Decimal'] = Decimal
        return simple_eval(decistmt(self.penalty_formula), **context)

    @classmethod
    def validate(cls, records):
        super(Party, cls).validate(records)
        for record in records:
            record.check_penalty_formula()
